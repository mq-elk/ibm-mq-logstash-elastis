#!/bin/bash

ELK_VERSION=7.6.2
MQ_VERSION=9.1.4.0-r1

param=$1

if [ -z $param ];then
    echo "Please execute command infra.sh [build | run | build-run | del]"
    echo "  ./infra.sh build-run "
    exit 1;
fi

if [ $param = "build" ]
 then

    echo "bulding docker images"
    docker build -t mg-ibmmq:9.1 --build-arg MQ_VERSION=$MQ_VERSION ./mq/
    docker build -t mg-elasticsearch:7.6 --build-arg ELK_VERSION=$ELK_VERSION ./elasticsearch/
    docker build -t mg-logstash:7.6 --build-arg ELK_VERSION=$ELK_VERSION ./logstash/
    docker build -t mg-kibanna:7.6 --build-arg ELK_VERSION=$ELK_VERSION  ./kibana/

elif [ $param = "run" ]
    then

        echo "run docker stack mq_tlf and create container mq_server"
        docker stack deploy -c docker-stack.yml mq_tlf
        docker run \
        --name mq_server \
        --env LICENSE=accept \
        --env MQ_QMGR_NAME=QM1 \
        --env MQ_APP_PASSWORD=clave2030 \
        --volume qm1data:/mnt/mqm \
        --publish 1414:1414 \
        --publish 9443:9443 \
        --network mq_tlf_elk \
        --network-alias qmgr \
        --detach \
        mg-ibmmq:9.1

elif [ $param = "build-run" ]
 then

    echo "bulding docker images"

    docker build -t mg-ibmmq:9.1 --build-arg MQ_VERSION=$MQ_VERSION ./mq/
    docker build -t mg-elasticsearch:7.6 --build-arg ELK_VERSION=$ELK_VERSION ./elasticsearch/
    docker build -t mg-logstash:7.6 --build-arg ELK_VERSION=$ELK_VERSION ./logstash/
    docker build -t mg-kibanna:7.6 --build-arg ELK_VERSION=$ELK_VERSION  ./kibana/

    echo "run docker stack mq_tlf and create container mq_server"

    docker stack deploy -c docker-stack.yml mq_tlf
    docker run \
    --name mq_server \
    --env LICENSE=accept \
    --env MQ_QMGR_NAME=QM1 \
    --env MQ_APP_PASSWORD=clave2030 \
    --volume qm1data:/mnt/mqm \
    --publish 1414:1414 \
    --publish 9443:9443 \
    --network mq_tlf_elk \
    --network-alias qmgr \
    --detach \
    mg-ibmmq:9.1

elif [ $param = "del" ]
    then
    echo "remove container and docker stack mq_tlf"
    docker container kill mq_server
    docker container rm mq_server
    docker stack rm mq_tlf
else
    echo "Option not found "
    echo "Please execute command infra.sh [build | run | build-run | del]"
    echo "  ./infra.sh build-run "
    exit 1;
fi;
